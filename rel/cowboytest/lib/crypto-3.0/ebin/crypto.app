%% app generated at {2013,8,21} {23,56,3}
{application,crypto,
             [{description,"CRYPTO version 2"},
              {vsn,"3.0"},
              {id,[]},
              {modules,[crypto,crypto_app,crypto_server,crypto_sup]},
              {registered,[crypto_sup,crypto_server]},
              {applications,[kernel,stdlib]},
              {included_applications,[]},
              {env,[]},
              {maxT,infinity},
              {maxP,infinity},
              {mod,{crypto_app,[]}}]}.

