%% app generated at {2013,8,21} {23,56,2}
{application,asn1,
             [{description,"The Erlang ASN1 compiler version 2.0.2"},
              {vsn,"2.0.2"},
              {id,[]},
              {modules,[asn1rt,asn1rt_nif]},
              {registered,[asn1_ns,asn1db]},
              {applications,[kernel,stdlib]},
              {included_applications,[]},
              {env,[]},
              {maxT,infinity},
              {maxP,infinity}]}.

